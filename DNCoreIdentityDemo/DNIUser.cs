﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNCoreIdentityDemo
{
    // previous stand alone user model
    /*public class DNIUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string PasswordHash { get; set; }
    }*/

    // We are extending IdentityUser, now we have what IdentityUser have and more :)
    //The table generated for user will be the default IdentityUser model table "[AspNetUsers]" . And Locale and OrgId column will be added to the same table.
    //Also Organization table is generated.

    public class DNIUser : IdentityUser
    {
        public string Locale { get; set; } = "en-GB";

        public string OrgId { get; set; }
    }

    public class Organization {
        public string Id { get; set; }
        public string Name { get; set; }

    }
}
