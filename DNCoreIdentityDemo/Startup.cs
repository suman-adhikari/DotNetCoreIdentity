﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DNCoreIdentityDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        /* public void ConfigureServices(IServiceCollection services) 
        {
            services.AddMvc();
            // after configuring EF, we must run migration:
            // open extennal cli: cmd, go to location of <projectname>.scproj
            // type dotnet ef migrations Initial : this will generate migration files to create database and tables
            // tables are not yet generated, we must run the next command : dotnet ef database update

            var connectionString = "Data Source=SUMANADHIKARI\\SQLSERVER; database=IdentityDemoEFDb; trusted_connection=yes;";
            //since EF is thirt party software, we must provide its assemble
            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<IdentityDbContext>(opt=>opt.UseSqlServer(connectionString, sql=>sql.MigrationsAssembly(migrationAssembly)));

            services.AddIdentityCore<IdentityUser>(options => { });
            // for UserStoreBase, ie using the identity's default user table/model :IdentityUser 
            // services.AddScoped<IUserStore<IdentityUser>, CustomIdentityUserStore>();

            // Here we are implementing the default EF user store ie UserOnlyStore<IdentityUser, IdentityDbContext>
            services.AddScoped<IUserStore<IdentityUser>, UserOnlyStore<IdentityUser, IdentityDbContext>>();
            services.AddAuthentication("cookies").AddCookie("cookies", options => options.LoginPath = "/Home/Login");
        }
        */

        // this config is purely for Extended IdentityUser with EF
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            var connectionString = "Data Source=SUMANADHIKARI\\SQLSERVER; database=PluralSiteUser; trusted_connection=yes;";
            var migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            services.AddDbContext<PluralSiteUserDbContext>(opt => opt.UseSqlServer(connectionString, sql => sql.MigrationsAssembly(migrationAssembly)));

            services.AddIdentityCore<DNIUser>(options => { });
            // Here we are implementing the default EF user store ie UserOnlyStore<IdentityUser, IdentityDbContext>
            services.AddScoped<IUserStore<DNIUser>, UserOnlyStore<DNIUser, PluralSiteUserDbContext>>();
            services.AddAuthentication("cookies").AddCookie("cookies", options => options.LoginPath = "/Home/Login");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseAuthentication();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
