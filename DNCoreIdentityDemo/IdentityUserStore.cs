﻿using Dapper;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DNCoreIdentityDemo
{
    public class IdentityUserStore : IUserStore<DNIUser>, IUserPasswordStore<DNIUser>
    {
        public async Task<IdentityResult> CreateAsync(DNIUser user, CancellationToken cancellationToken)
        {
            using (var connection = GetOpenConnection())
            {
                await connection.ExecuteAsync("insert into PluralsightUsers(Id,Username,normalizedUserName,passwordhash)" +
                    "values(@id,@username,@normalizedUserName,@passwordhash)",
                    new
                    {
                        id = user.Id,
                        username = user.UserName,
                        normalizedUserName = user.NormalizedUserName,
                        passwordhash = user.PasswordHash
                    }
                );
            }
            return IdentityResult.Success;
        }

        public Task<IdentityResult> DeleteAsync(DNIUser user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public static DbConnection GetOpenConnection()
        {
            var connection = new SqlConnection("Data Source=SUMANADHIKARI\\SQLSERVER; database=IdentityDemoDb; trusted_connection=yes;");
            connection.Open();
            return connection;
        }


        public void Dispose()
        {

        }

        public async Task<DNIUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            using (var con = GetOpenConnection())
            {
                return await con.QueryFirstOrDefaultAsync<DNIUser>("select * From PluralsightUsers where Id = @id", new { id = userId });
            }
        }

        public async Task<DNIUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            using (var con = GetOpenConnection())
            {
                return await con.QueryFirstOrDefaultAsync<DNIUser>("select * from PluralsightUsers where normalizedUserName=@username", new { username = normalizedUserName });
            }
        }

        public Task<string> GetNormalizedUserNameAsync(DNIUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.NormalizedUserName);
        }

        public Task<string> GetUserIdAsync(DNIUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.Id);
        }

        public Task<string> GetUserNameAsync(DNIUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.UserName);
        }

        public Task SetNormalizedUserNameAsync(DNIUser user, string normalizedName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = normalizedName;
            return Task.CompletedTask;
        }

        public Task SetUserNameAsync(DNIUser user, string userName, CancellationToken cancellationToken)
        {
            user.NormalizedUserName = userName;
            return Task.CompletedTask;
        }

        public async Task<IdentityResult> UpdateAsync(DNIUser user, CancellationToken cancellationToken)
        {
            using (var con = GetOpenConnection())
            {
                await con.ExecuteAsync("update PluralSightUsers " +
                    "set id=@id," +
                    "username=@username," +
                    "normalizedUserName=@normalizedUserName," +
                    "passwordHash=@passwordHash" +
                    "where id=@id",
                     new
                     {
                         id = user.Id,
                         userName = user.UserName,
                         normalizedUserName = user.UserName,
                         passwordHash = user.PasswordHash
                     }
                    );
            }
            return IdentityResult.Success;
        }

        public Task SetPasswordHashAsync(DNIUser user, string passwordHash, CancellationToken cancellationToken)
        {
            user.PasswordHash = passwordHash;
            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(DNIUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(DNIUser user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.PasswordHash != null);
        }
    }
}
