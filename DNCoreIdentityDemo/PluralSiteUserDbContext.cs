﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DNCoreIdentityDemo
{
    // This is for telling, Give me all from the EF default's store but the model with be my own model not EF's IdentityUser. BTW i am extending IdentityUser
    // if we were to use EF's default IdentityUser, we dont even need this class.
    public class PluralSiteUserDbContext : IdentityDbContext<DNIUser>
    {
        public PluralSiteUserDbContext(DbContextOptions<PluralSiteUserDbContext> options): base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            // add all the default EF tables plus add this one too :)
            builder.Entity<DNIUser>(user => user.HasIndex(x=>x.Locale).IsUnique(false));

            builder.Entity<Organization>(org =>
            {
                // create one more table
                org.ToTable("Organization");
                org.HasKey(x => x.Id);
                //the organization has many DNIUsers but user have just one organization.DNIUser has foreign key OrgId but is not required.
                org.HasMany<DNIUser>().WithOne().HasForeignKey(x => x.OrgId).IsRequired(false);
            });
        }
    }
}
